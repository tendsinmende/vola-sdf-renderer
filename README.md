<div align="center">

# Vola-SDF-Renderer

Vulkan and [rust-gpu](https://github.com/EmbarkStudios/rust-gpu) test renderer for [Vola](https://gitlab.com/tendsinmende/vola).

[![dependency status](https://deps.rs/repo/gitlab/tendsinmende/vola-sdf-renderer/status.svg)](https://deps.rs/repo/gitlab/tendsinmende/vola-sdf-renderer)
[![MPL 2.0](https://img.shields.io/badge/License-MPL_2.0-blue)](LICENSE)

</div>

## Disclaimer

The whole thing is highly experimental at the moment. Don't use it for anything serious.

## Dependencies

- Khronos validation layers
- spirv-tools (speciffically `spirv-link`)



## Building

✨ _Just run `cargo build`_ ✨

### Running
Application that runtime patches a SDF function of a SPIR-V shader from time to time
``` shell
cargo run --bin renderer
```

Then, at runtime, modify `vola_src/sdf.vola`. Whenever you safe the file, the patcher will kick in and try to compile the new source-code.
If something goes wrong, you'll see that in your terminal.

### Keybindings

-Press right-click to enter camera-edit. Moving the mouse will turn the camera, `WASD+EQ` moves the camera.
- The offset parameter can be changed at any time via `IJKL+UO` .
- Scrolling changes the render distance.
- `Ctrl` switches the gradient technique (numeric and analytic)
- `C` shows the difference of both gradient techniques
- `Num1` / `Num2` switches sphere-tracing and segment-tracing-approximation
- `Tab` Shows ray-tracing step-count heatmap


### Debugging
You can set `VOLA_BACKTRACE=1` to print a backtrace whenever an error is reported.

Prepend `VOLA_DUMP_ALL=1 VOLA_DUMP_VIEWER=1` to get rvsdg-viewer files whenever patching. Note that this'll make the patcher slow (>20s).
```shell
VOLA_DUMP_ALL=1 VOLA_DUMP_VIEWER=1 cargo run --bin renderer --release
```

If you are only interested in the final optimizer use `VOLA_OPT_FINAL` instead of `VOLA_DUMP_ALL`.

The resulting binary files can be opened via the `rvsdg-viewer-ui` of the [Vola](https://gitlab.com/tendsinmende/vola) project like this:
```shell
cargo run --bin rvsdg-viewer-ui -- path/to/the/binary/file.bin
```


## License

Licensed under

Mozilla Public License Version 2.0 ([LICENSE](LICENSE) or <https://www.mozilla.org/en-US/MPL/2.0/>)


### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, is licensed under the Mozilla Public License Version 2.0.
