#![no_std]
#![feature(asm_experimental_arch)]

use core::f32;

use shared::glam::{vec2, vec3, vec4};
#[cfg(target_arch = "spirv")]
use shared::spirv_std::num_traits::Float;
use shared::spirv_std::{self, Sampler, TypedBuffer};
use shared::spirv_std::{spirv, Image, RuntimeArray};
use shared::{Ray, ITER_BUFFER_SIZE};
use spirv_std::glam::{IVec2, UVec2, UVec3, Vec2, Vec3, Vec3Swizzles, Vec4, Vec4Swizzles};

const EPS: f32 = 0.0001;
const MINT: f32 = 0.1;
const MAXT: f32 = 500.0;
const MAX_I: usize = 1024;

//snacked from https://www.shadertoy.com/view/WlfXRN under CC0
fn viridis(t: f32) -> Vec3 {
    const c0: Vec3 = vec3(0.2777273272234177, 0.005407344544966578, 0.3340998053353061);
    const c1: Vec3 = vec3(0.1050930431085774, 1.404613529898575, 1.384590162594685);
    const c2: Vec3 = vec3(-0.3308618287255563, 0.214847559468213, 0.09509516302823659);
    const c3: Vec3 = vec3(-4.634230498983486, -5.799100973351585, -19.33244095627987);
    const c4: Vec3 = vec3(6.228269936347081, 14.17993336680509, 56.69055260068105);
    const c5: Vec3 = vec3(4.776384997670288, -13.74514537774601, -65.35303263337234);
    const c6: Vec3 = vec3(-5.435455855934631, 4.645852612178535, 26.3124352495832);

    return c0 + t * (c1 + t * (c2 + t * (c3 + t * (c4 + t * (c5 + t * c6)))));
}

//ULTRA VIOLET
//const FOG_COLOR: Vec3 = vec3(95.0 / 255.0, 75.0 / 255.0, 139.0 / 255.0);
const FOG_COLOR: Vec3 = vec3(56.0 / 255.0, 52.0 / 255.0, 49.0 / 255.0);

fn luminance_rec_709(rgb: Vec3) -> f32 {
    rgb.dot(Vec3::new(0.2126, 0.7152, 0.0722))
}

fn reinhard_map(hdr: Vec4) -> Vec4 {
    (hdr.xyz() / (1.0 + luminance_rec_709(hdr.xyz()))).extend(hdr.w)
}

fn reinhard_inverse(ldr: Vec4) -> Vec4 {
    (ldr.xyz() / (1.0 - luminance_rec_709(ldr.xyz()))).extend(ldr.w)
}

//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_sdf"]
pub fn eval_sdf(pos: Vec3, offset: Vec3, time: f32) -> f32 {
    let dist = (pos - offset).length() - (1.0 * time.sin());
    dist
}
//The Sdf function we are patching
#[inline(never)]
#[export_name = "eval_albedo"]
pub fn eval_albedo(pos: Vec3, offset: Vec3, time: f32) -> Vec3 {
    (pos + offset) / 2.0 * time.sin()
}

//The Sdf function we are patching to get the _normal_ vector.
#[inline(never)]
#[export_name = "eval_gradient"]
pub fn eval_gradient(pos: Vec3, offset: Vec3, time: f32) -> Vec3 {
    (pos + offset) / 2.0 * time.sin()
}

pub fn eval_nrm(pos: Vec3, offset: Vec3, time: f32) -> Vec3 {
    let grad = eval_gradient(pos, offset, time);
    grad / grad.length()
}

//Uses the thetrahedron technique described here: https://iquilezles.org/articles/normalsSDF/
fn calc_normal(at: Vec3, offset: Vec3, time: f32) -> Vec3 {
    const H: f32 = 0.00001;
    (vec3(1.0, -1.0, -1.0) * eval_sdf(at + (H * vec3(1.0, -1.0, -1.0)), offset, time)
        + vec3(-1.0, -1.0, 1.0) * eval_sdf(at + (H * vec3(-1.0, -1.0, 1.0)), offset, time)
        + vec3(-1.0, 1.0, -1.0) * eval_sdf(at + (H * vec3(-1.0, 1.0, -1.0)), offset, time)
        + vec3(1.0, 1.0, 1.0) * eval_sdf(at + (H * vec3(1.0, 1.0, 1.0)), offset, time))
    .normalize()
}

fn calc_gradient(at: Vec3, offset: Vec3, time: f32) -> Vec3 {
    const V1: Vec3 = vec3(1.0, 0.0, 0.0);
    const V2: Vec3 = vec3(0.0, 1.0, 0.0);
    const V3: Vec3 = vec3(0.0, 0.0, 1.0);

    (Vec3::new(
        eval_sdf(at + V1 * EPS, offset, time),
        eval_sdf(at + V2 * EPS, offset, time),
        eval_sdf(at + V3 * EPS, offset, time),
    ) - Vec3::new(
        eval_sdf(at - V1 * EPS, offset, time),
        eval_sdf(at - V2 * EPS, offset, time),
        eval_sdf(at - V3 * EPS, offset, time),
    )) / (2.0 * EPS)
}

fn fresnel(u: f32, f0: Vec3) -> Vec3 {
    f0 + (Vec3::ONE - f0) * (1.0 - u).powf(5.0)
}

fn gradient_dir_abs(pos: Vec3, dir: Vec3, push: &shared::RenderUniform) -> f32 {
    let a = eval_sdf(pos + dir * EPS, push.offset.into(), push.time);
    let b = eval_sdf(pos - dir * EPS, push.offset.into(), push.time);
    (a - b) / (2.0 * EPS)
}

fn scene_k(start: Vec3, end: Vec3, dir: Vec3, push: &shared::RenderUniform) -> (f32, f32) {
    let dist = eval_sdf(start, push.offset.into(), push.time);

    if push.use_numeric_normal == 1 {
        let gs = calc_gradient(start, push.offset.into(), push.time);
        let ge = calc_gradient(end, push.offset.into(), push.time);

        let fds = gs.dot(dir).abs();
        let fde = ge.dot(dir).abs();

        let lam = fds.max(fde);

        (lam, dist)
    } else {
        let gs = eval_gradient(start, push.offset.into(), push.time);
        let ge = eval_gradient(end, push.offset.into(), push.time);

        let fds = gs.dot(dir).abs();
        let fde = ge.dot(dir).abs();
        //let fds = gradient_dir_abs(start, dir, push);
        //let fde = gradient_dir_abs(end, dir, push);

        let lam = fds.max(fde);

        (lam, dist)
    }
}

///Segment tracing approximation based on https://www.shadertoy.com/view/wdGyDD
fn segment_tracing(ray: &Ray, push: &shared::RenderUniform) -> (usize, f32) {
    const GRID_SIZE: f32 = 0.4;
    const GLOBAL_K: f32 = 1.25;

    let mint = MINT;
    let maxt = ray.max_t;
    let mut t = mint;

    let c = 1.5;
    let mut ts = maxt - mint;
    ts = ts.min(GRID_SIZE);
    let mut i = 0;

    while t < ray.max_t && i < MAX_I {
        let pt = ray.at(t);
        let pts = ray.at(t + ts);
        let (k, dist) = scene_k(pt, pts, ray.direction, push);

        if dist < EPS {
            return (i, t);
        }
        let mut tk = dist.abs() / k.max(0.01);
        tk = (dist.abs() / GLOBAL_K).max(tk.min(ts));
        ts = tk;

        if tk >= 0.0 {
            t += tk.max(EPS);
        }
        ts = tk * c;
        ts = ts.min(GRID_SIZE);

        if push.use_numeric_normal == 1 {
            //numeric uses 6 per segment plus one initial value
            i += 13;
        } else {
            //analytic segment trace uses 3 lookups, one initial, and the two
            //gradient evaluations
            i += 3;
        }

        if t > maxt {
            break;
        }
    }

    (i, f32::INFINITY)
}

fn sphere_tracing(ray: &Ray, push: &shared::RenderUniform) -> (usize, f32) {
    let mut t = MINT;
    let mut i = 0;

    while t < ray.max_t && i < MAX_I {
        let res = eval_sdf(ray.at(t), Vec3::from(push.offset), push.time);
        if res < EPS {
            return (i, t);
        } else {
            t += res.max(EPS);
        }
        i += 1;
    }

    (i, f32::INFINITY)
}

#[spirv(compute(threads(8, 8, 1)))]
pub fn renderer(
    #[spirv(push_constant)] push: &shared::RenderUniform,
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(descriptor_set = 1, binding = 0)] rgbaf32_images: &RuntimeArray<
        Image!(2D, format = rgba32f, sampled = false),
    >,
    #[spirv(descriptor_set = 0, binding = 0, storage_buffer)] itercounter: &mut RuntimeArray<
        TypedBuffer<[u32; ITER_BUFFER_SIZE]>,
    >,
) {
    let coord = id.xy();
    if coord.x >= push.resolution[0] || coord.y >= push.resolution[1] {
        return;
    }
    let coordf32 = coord.as_vec2();
    let coord_uv = coordf32 / UVec2::new(push.resolution[0], push.resolution[1]).as_vec2();

    let ndc = coord_uv * 2.0 - 1.0;
    let ray = push.ray_from_ndc(ndc);

    let (i, t) = match push.tracing_mode {
        1 => segment_tracing(&ray, push),
        _ => sphere_tracing(&ray, push),
    };

    if push.iter_buffer.is_valid() {
        let index = push.resolution[0] * coord.y + coord.x;
        unsafe {
            itercounter.index_mut(push.iter_buffer.index() as usize)[index as usize] = i as u32;
        }
    }

    if push.render_iterations > 0 {
        let iter_t = i as f32 / MAX_I as f32;
        let color = viridis(iter_t.clamp(0.0, 1.0));
        unsafe {
            rgbaf32_images
                .index(push.target_image.index() as usize)
                .write(coord, color.extend(1.0));
        }
        return;
    }
    /* visualizes the k and dist value
        if push.tracing_mode == 1 {
            let d2 = scene_k(ray.at(t), ray.at(t), ray.direction, push);

            unsafe {
                rgbaf32_images
                    .index(push.target_image.index() as usize)
                    .write(coord, Vec2::from(d2).extend(1.0).extend(1.0));
            }
            return;
        }
    */
    let fog_base = (t / ray.max_t).clamp(0.0, 1.0);
    let fog_color = FOG_COLOR;

    if i >= MAX_I {
        unsafe {
            rgbaf32_images
                .index(push.target_image.index() as usize)
                .write(coord, Vec3::X.extend(1.0));
        }
        return;
    }

    //Early out as _sky_ if we ended the ray
    if t > ray.max_t {
        unsafe {
            rgbaf32_images
                .index(push.target_image.index() as usize)
                .write(coord, reinhard_map(fog_color.extend(1.0)));
        }
        return;
    }

    let nrm = if push.use_numeric_normal == 1 {
        calc_normal(ray.at(t), Vec3::from(push.offset), push.time)
    } else {
        eval_nrm(ray.at(t), Vec3::from(push.offset), push.time)
    };

    //NOTE: Flipping cause we are in Vulkan space with -Y == UP.
    const LIGHT_DIR: Vec3 = vec3(1.0, -1.0, 1.0);

    /*let base_color = if (((t / ray.max_t) * 10.0) as i32) % 2 == 1 {
        Vec3::new(1.0, 1.0, 1.0)
    } else {
        Vec3::new(1.0, 0.5, 0.5)
    };*/

    let base_color = eval_albedo(ray.at(t), Vec3::from(push.offset), push.time);

    let n_dot_l = LIGHT_DIR.dot(nrm);
    let ao = eval_sdf(ray.at(t) + nrm * 0.2, Vec3::from(push.offset), push.time);

    let rim_light = Vec3::splat(1.0 - nrm.dot(-ray.direction)) * base_color * 0.2;
    let direct_light = Vec3::new(1.0, 1.0, 1.0) * n_dot_l.max(0.1);
    let color = base_color * (direct_light + rim_light) * (ao / 0.2);
    let color = color.lerp(FOG_COLOR, fog_base);

    let color = if push.color_diff == 1 {
        let calc = calc_normal(ray.at(t), Vec3::from(push.offset), push.time);
        let eval = eval_nrm(ray.at(t), Vec3::from(push.offset), push.time);

        (eval - calc).abs()
    } else {
        color
    };

    if push.target_image.is_valid() {
        unsafe {
            rgbaf32_images
                .index(push.target_image.index() as usize)
                .write(coord, reinhard_map(color.extend(1.0)));
        }
    }
}
