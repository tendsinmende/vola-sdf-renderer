use std::time::Instant;

use shared::glam::Vec3;
use winit::{
    event::{ElementState, Event, KeyEvent, WindowEvent},
    keyboard::{KeyCode, PhysicalKey},
};

pub struct OffsetEntity {
    pub offset_parameter: Vec3,
    offset_velocity: Vec3,
    last_update: Instant,
}

impl OffsetEntity {
    const BOUNCE: f32 = 1.0;
    const SLOW_DOWN: f32 = 0.5;
    const STOP: f32 = 0.1;
    pub fn on_event(&mut self, event: &Event<()>) {
        match event {
            Event::WindowEvent {
                event:
                    WindowEvent::KeyboardInput {
                        event:
                            KeyEvent {
                                physical_key: PhysicalKey::Code(keycode),
                                state,
                                ..
                            },
                        ..
                    },
                ..
            } => match (keycode, state) {
                (KeyCode::KeyJ, ElementState::Released) => self.offset_velocity.x = -Self::BOUNCE,
                (KeyCode::KeyL, ElementState::Released) => {
                    self.offset_velocity.x = Self::BOUNCE;
                }

                (KeyCode::KeyK, ElementState::Released) => self.offset_velocity.z = -Self::BOUNCE,
                (KeyCode::KeyI, ElementState::Released) => self.offset_velocity.z = Self::BOUNCE,

                (KeyCode::KeyO, ElementState::Released) => {
                    self.offset_velocity.y = -Self::BOUNCE;
                }
                (KeyCode::KeyU, ElementState::Released) => {
                    self.offset_velocity.y = Self::BOUNCE;
                }

                _ => {}
            },
            _ => {}
        }
    }

    pub fn update(&mut self) {
        let delta = self.last_update.elapsed().as_secs_f32();
        self.last_update = Instant::now();

        self.offset_parameter += self.offset_velocity * delta;

        self.offset_velocity /= 1.0 + (delta * Self::SLOW_DOWN);
        for i in 0..3 {
            if self.offset_velocity[i].abs() < Self::STOP {
                self.offset_velocity[i] = 0.0;
            }
        }
    }

    pub fn new() -> Self {
        OffsetEntity {
            offset_parameter: Vec3::ZERO,
            offset_velocity: Vec3::ZERO,
            last_update: Instant::now(),
        }
    }
}
