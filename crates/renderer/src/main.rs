//! A really simple sphere trace renderer.
//!
//! uses the `sdf.vola` file to runtime-patch the sphere-tracing shader with new code.
//!
//! This is a test intersection of two projects. The [vola](https://www.gitlab.com/tendsinmende/vola) compiler, and the [spv-patcher](https://www.gitlab.com/tendsinmende/spv-patcher).

use std::{
    error::Error,
    time::{Duration, Instant},
    u32,
};

use camera::Camera;
use iterprofiler::IterProfiler;
use marpii::{ash::vk::Extent2D, context::Ctx, resources::Buffer};
use marpii_rmg::{BufferHandle, Rmg, Task};
use marpii_rmg_tasks::{DownloadBuffer, SwapchainPresent};
use offset_entity::OffsetEntity;
use winit::{
    application::ApplicationHandler,
    event::{
        DeviceEvent, ElementState, Event, KeyEvent, MouseButton, MouseScrollDelta, WindowEvent,
    },
    event_loop::ControlFlow,
    keyboard::{KeyCode, PhysicalKey},
    window::{CursorGrabMode, Window, WindowAttributes, WindowButtons},
};

mod camera;
mod offset_entity;
mod patcher;
mod st_pass;

mod iterprofiler;

enum App {
    Loading,
    Running {
        rmg: Rmg,
        window: Window,

        present_pass: SwapchainPresent,
        stpass: st_pass::SphereTracing,
        iterbuffer: BufferHandle<u32>,
        itercount_download_pass: DownloadBuffer<u32>,
        iter_recorder: IterProfiler,
        camera: Camera,
        offset_entity: OffsetEntity,
        start: Instant,
        last_fps_draw: Instant,
        is_camera_captured: bool,
        max_t: u32,
    },
}

impl App {
    pub fn handle_event(&mut self, event: Event<()>) {
        if let App::Running {
            rmg: _,
            window: _,
            present_pass: _,
            stpass: _,
            iterbuffer: _,
            itercount_download_pass: _,
            iter_recorder: _,
            camera,
            offset_entity,
            start: _,
            last_fps_draw: _,
            is_camera_captured,
            max_t: _,
        } = self
        {
            if *is_camera_captured {
                camera.on_event(&event);
            }

            offset_entity.on_event(&event);
        }
    }
}

impl ApplicationHandler for App {
    fn resumed(&mut self, event_loop: &winit::event_loop::ActiveEventLoop) {
        match self {
            Self::Loading => {
                //initialize the app
                let window = event_loop
                    .create_window(
                        WindowAttributes::default()
                            .with_title("Vola Renderer")
                            .with_enabled_buttons(WindowButtons::CLOSE)
                            .with_resizable(true),
                    )
                    .expect("failed to open window");

                let (context, surface) = Ctx::default_with_surface(&window, true).unwrap();
                let mut rmg = Rmg::new(context).unwrap();

                let iter_element_count = window.inner_size().width * window.inner_size().height;
                let iter_buffer = rmg
                    .new_buffer(iter_element_count as usize, Some("Iter Buffer"))
                    .unwrap();
                let download_buffer = DownloadBuffer::new(&mut rmg, iter_buffer.clone()).unwrap();
                let present_pass = SwapchainPresent::new(&mut rmg, surface).unwrap();
                let stpass = st_pass::SphereTracing::new(
                    &mut rmg,
                    present_pass.extent().unwrap_or(Extent2D {
                        width: 1,
                        height: 1,
                    }),
                );

                let camera = Camera::default();
                let offset_entity = OffsetEntity::new();

                *self = Self::Running {
                    rmg,
                    window,
                    present_pass,
                    stpass,
                    iterbuffer: iter_buffer,
                    itercount_download_pass: download_buffer,
                    iter_recorder: IterProfiler::new(),
                    camera,
                    offset_entity,
                    start: Instant::now(),
                    last_fps_draw: Instant::now(),
                    is_camera_captured: false,
                    max_t: 20,
                };
            }
            //already running
            Self::Running { .. } => {}
        }
    }

    fn device_event(
        &mut self,
        _event_loop: &winit::event_loop::ActiveEventLoop,
        device_id: winit::event::DeviceId,
        event: DeviceEvent,
    ) {
        //wrap event and sent off to handler
        let hlevent = Event::DeviceEvent {
            device_id,

            event: event.clone(),
        };
        self.handle_event(hlevent);
    }

    fn window_event(
        &mut self,
        event_loop: &winit::event_loop::ActiveEventLoop,
        window_id: winit::window::WindowId,
        event: WindowEvent,
    ) {
        //wrap event and sent off to handler
        let hlevent = Event::WindowEvent {
            window_id,
            event: event.clone(),
        };
        self.handle_event(hlevent);

        match self {
            Self::Loading => {}
            Self::Running {
                rmg,
                window,
                present_pass,
                stpass,
                iterbuffer,
                itercount_download_pass,
                iter_recorder,
                camera,
                offset_entity,
                start,
                last_fps_draw,
                is_camera_captured,
                max_t,
            } => {
                match event {
                    //If close requested, or esc pressed
                    WindowEvent::CloseRequested
                    | WindowEvent::KeyboardInput {
                        event:
                            KeyEvent {
                                physical_key: PhysicalKey::Code(KeyCode::Escape),
                                state: ElementState::Released,
                                ..
                            },
                        ..
                    } => {
                        event_loop.exit();
                    }
                    WindowEvent::Resized(new_size) => {
                        if new_size.height == 0 || new_size.width == 0 {
                            return;
                        }
                        let element_count = new_size.width * new_size.height;

                        let iter_buffer = rmg
                            .new_buffer(element_count as usize, Some("Iter Buffer"))
                            .unwrap();
                        let download_buffer =
                            DownloadBuffer::new(rmg, iter_buffer.clone()).unwrap();
                        *iterbuffer = iter_buffer;
                        *itercount_download_pass = download_buffer;
                    }
                    WindowEvent::RedrawRequested => {
                        camera.update();
                        offset_entity.update();

                        stpass.update_camera(&camera);
                        stpass.offset_parameter(offset_entity.offset_parameter);
                        stpass.time(start.elapsed().as_secs_f32());
                        let resolution = window.inner_size();
                        stpass.notify_resolution(
                            rmg,
                            Extent2D {
                                width: resolution.width,
                                height: resolution.height,
                            },
                        );
                        stpass.pc.get_content_mut().iter_buffer = rmg
                            .resources
                            .resource_handle_or_bind(iterbuffer.clone())
                            .unwrap();
                        stpass.pc.get_content_mut().max_t = iter_recorder.get_maxt();

                        present_pass.push_image(
                            stpass.target_image.clone(),
                            stpass.target_image.extent_2d(),
                        );

                        let mut recorder = rmg
                            .record()
                            .add_task(stpass)
                            .unwrap()
                            .add_task(itercount_download_pass)
                            .unwrap();

                        recorder = match recorder.add_task(present_pass) {
                            Err(e) => {
                                println!("Swapchain error: {e:?}");
                                return;
                            }
                            Ok(rec) => rec,
                        };

                        recorder.execute().unwrap();

                        if last_fps_draw.elapsed() > Duration::from_millis(500) {
                            rmg.wait_for_idle().unwrap();
                            let timing = rmg.get_recent_track_timings();
                            for t in timing {
                                if &t.name == stpass.name() {
                                    let timing_ms = t.timing / 1_000_000.0;
                                    println!(
                                        "{}: {}ms  aka {}fps",
                                        t.name,
                                        timing_ms,
                                        1.0 / (timing_ms / 1000.0)
                                    );

                                    *last_fps_draw = Instant::now();
                                }
                            }
                        }

                        if iter_recorder.is_recording() {
                            rmg.wait_for_idle().unwrap();
                            let element_count = (resolution.width * resolution.height) as usize;
                            iter_recorder.buffer.resize(element_count, u32::MAX);
                            match itercount_download_pass.download(rmg, &mut iter_recorder.buffer) {
                                Ok(downloads) => {
                                    iter_recorder.push_first_elements(downloads);
                                }
                                Err(e) => {
                                    println!("Failed to download buffer: {}", e);
                                }
                            }
                        }

                        window.request_redraw();
                    }

                    WindowEvent::MouseInput {
                        state,
                        button: MouseButton::Right,
                        ..
                    } => match state {
                        ElementState::Released => {
                            *is_camera_captured = false;
                            if let Err(e) = window.set_cursor_grab(CursorGrabMode::None) {
                                log::error!("{e}");
                            }
                        }
                        ElementState::Pressed => {
                            *is_camera_captured = true;
                            if let Err(e) = window.set_cursor_grab(CursorGrabMode::None) {
                                log::error!("{e}");
                            }
                        }
                    },
                    WindowEvent::MouseWheel { delta, .. } => {
                        let delta = match delta {
                            MouseScrollDelta::LineDelta(x, y) => y as isize,
                            MouseScrollDelta::PixelDelta(at) => at.y as isize,
                        };
                        iter_recorder.change_maxt(delta);
                    }

                    WindowEvent::KeyboardInput {
                        event:
                            KeyEvent {
                                physical_key: PhysicalKey::Code(keycode),
                                state,
                                ..
                            },
                        ..
                    } => {
                        //On pressing "ctrl", switch normal calculation
                        match (keycode, state) {
                            (KeyCode::ControlLeft, ElementState::Released) => {
                                if stpass.pc.get_content().use_numeric_normal == 0 {
                                    stpass.pc.get_content_mut().use_numeric_normal = 1;

                                    println!(
                                        "Using Numeric normals: {}!",
                                        stpass.pc.get_content().use_numeric_normal
                                    );
                                } else {
                                    stpass.pc.get_content_mut().use_numeric_normal = 0;

                                    println!(
                                        "Using AD Normals: {}!",
                                        stpass.pc.get_content().use_numeric_normal
                                    );
                                }
                            }
                            (KeyCode::Tab, ElementState::Released) => {
                                if stpass.pc.get_content().render_iterations == 0 {
                                    println!("Rendering iterations!");
                                    stpass.pc.get_content_mut().render_iterations = 50;
                                } else {
                                    println!("Normal rendering!");
                                    stpass.pc.get_content_mut().render_iterations = 0;
                                }
                            }

                            (KeyCode::KeyC, ElementState::Released) => {
                                if stpass.pc.get_content().color_diff == 0 {
                                    println!("Show difference!");
                                    stpass.pc.get_content_mut().color_diff = 1;
                                } else {
                                    println!("Normal rendering!");
                                    stpass.pc.get_content_mut().color_diff = 0;
                                }
                            }

                            (KeyCode::KeyR, ElementState::Released) => {
                                if !iter_recorder.is_recording() {
                                    println!("Start recording frames!");
                                    iter_recorder.start_record();
                                }
                            }

                            (KeyCode::Digit1 | KeyCode::Numpad1, ElementState::Released) => {
                                println!("Using Sphere tracing!");
                                stpass.pc.get_content_mut().tracing_mode = 0;
                            }
                            (KeyCode::Digit2 | KeyCode::Numpad2, ElementState::Released) => {
                                println!("Using Segment tracing!");
                                stpass.pc.get_content_mut().tracing_mode = 1;
                            }
                            (KeyCode::Digit3 | KeyCode::Numpad3, ElementState::Released) => {
                                println!("Using  tracing!");
                                stpass.pc.get_content_mut().tracing_mode = 1;
                            }
                            _ => {}
                        }
                    }
                    _ => {}
                }
            }
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    simple_logger::SimpleLogger::new()
        .with_level(log::LevelFilter::Error)
        .init()
        .unwrap();

    let ev = winit::event_loop::EventLoop::new().expect("Failed to launch event loop");

    let mut app = App::Loading;
    //Poll for active rendering.
    ev.set_control_flow(ControlFlow::Poll);
    ev.run_app(&mut app).map_err(|e| e.into())
}
