use std::{fmt::Display, io::BufWriter, path::Path};

use ahash::AHashMap;
use serde::{Deserialize, Serialize};

#[derive(Serialize)]
pub struct Profile {
    min: usize,
    max: usize,
    avg: f32,
}

impl Display for Profile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}, {}, {}", self.min, self.max, self.avg)
    }
}

pub enum ProfilerState {
    Inactive(u32),
    Recording {
        maxt: u32,
        iteration: usize,
        before_t: u32,
    },
}

#[derive(Serialize)]
pub struct IterProfiler {
    #[serde(skip_serializing)]
    pub iterations_per_state: usize,
    #[serde(skip_serializing)]
    pub tstep_size: u32,
    #[serde(skip_serializing)]
    pub max_t_step: u32,
    ///Recorded profiles
    records: AHashMap<u32, Vec<Profile>>,
    #[serde(skip_serializing)]
    state: ProfilerState,
    #[serde(skip_serializing)]
    //Where stuff is recorded to.
    pub buffer: Vec<u32>,
}

impl IterProfiler {
    pub fn new() -> Self {
        Self {
            iterations_per_state: 10,
            tstep_size: 10,
            max_t_step: 500,
            records: AHashMap::default(),
            state: ProfilerState::Inactive(20),
            buffer: Vec::new(),
        }
    }

    fn write_results(&self) {
        if Path::new("result.ron").exists() {
            std::fs::remove_file("result.ron").unwrap();
        };
        let file = std::fs::File::create("result.ron").unwrap();
        let mut file_writer = BufWriter::new(file);
        ron::ser::to_writer(&mut file_writer, self).unwrap();
    }

    pub fn start_record(&mut self) {
        self.records.clear();
        let before = match self.state {
            ProfilerState::Inactive(b) => b,
            ProfilerState::Recording { before_t, .. } => before_t,
        };
        self.state = ProfilerState::Recording {
            maxt: 5,
            iteration: 0,
            before_t: before,
        };
    }

    pub fn change_maxt(&mut self, change: isize) {
        match &mut self.state {
            ProfilerState::Inactive(t) => *t = (*t as isize + change).max(5) as u32,
            ProfilerState::Recording { before_t, .. } => {
                *before_t = (*before_t as isize + change).max(5) as u32
            }
        }
    }

    pub fn get_maxt(&mut self) -> u32 {
        match &mut self.state {
            ProfilerState::Inactive(i) => *i,
            ProfilerState::Recording { maxt, .. } => *maxt,
        }
    }

    pub fn is_recording(&self) -> bool {
        if let ProfilerState::Recording { .. } = &self.state {
            true
        } else {
            false
        }
    }

    pub fn push_first_elements(&mut self, elements: usize) {
        match &mut self.state {
            ProfilerState::Inactive(_) => {}
            ProfilerState::Recording {
                maxt,
                iteration,
                before_t,
            } => {
                if !self.records.contains_key(&maxt) {
                    self.records.insert(*maxt, Vec::new());
                }

                let (max, min, all_iterations) = self.buffer[0..elements].into_iter().fold(
                    (0usize, usize::MAX, 0usize),
                    |(max, min, all), this| {
                        if *this == u32::MAX {
                            println!("Invalid timing :(");
                            (max, min, all)
                        } else {
                            (
                                max.max(*this as usize),
                                min.min(*this as usize),
                                all + *this as usize,
                            )
                        }
                    },
                );
                let avg = all_iterations as f32 / elements as f32;
                println!("Push[{maxt}]: {max}");
                self.records
                    .get_mut(&maxt)
                    .unwrap()
                    .push(Profile { min, max, avg });

                *iteration += 1;
                if *iteration > self.iterations_per_state {
                    *maxt += self.tstep_size;
                    *iteration = 0;
                };

                //Reset to inactive once finished
                let before_t = *before_t;
                if *maxt > self.max_t_step {
                    self.write_results();
                    self.state = ProfilerState::Inactive(before_t)
                }
            }
        }
    }
}
