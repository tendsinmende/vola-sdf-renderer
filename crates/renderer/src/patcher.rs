use hotwatch::{notify::Event, Hotwatch};
use marpii::{resources::ShaderModule, OoS};
use patch_link::{FuncIdent, MakeFunctionImport};
use spv_patcher::rspirv::{self, binary::Assemble, dr::Module};
use std::{
    path::{Path, PathBuf},
    sync::{
        mpsc::{Receiver, TryRecvError},
        Arc,
    },
    time::{Duration, Instant},
};

static BASE_SHADER: &'static [u8] = include_bytes!("../../resources/base-shader.spv");

///Patcher utility. Observes the `sdf.vola` file, and recompiles the shader if needed.
pub struct Patcher {
    ///Receiver of new shader modules
    recv: Receiver<OoS<ShaderModule>>,
    #[allow(dead_code)]
    hotwatch: Hotwatch,
}

struct Linker {
    src_file: PathBuf,
}

impl Linker {
    pub fn new(module: &Module) -> Self {
        let src_file = PathBuf::from("basecode.spv");
        Self::write_module_to_file(&src_file, module);
        Self { src_file }
    }

    fn write_module_to_file(file: &dyn AsRef<Path>, module: &Module) {
        let code = module.assemble();
        std::fs::write(file, bytemuck::cast_slice(&code)).unwrap();
    }

    ///Links `file` into `self.src_file`. If successful, returns the newly
    ///created module
    pub fn link_new(&self, module: &Vec<u8>) -> Option<Module> {
        const TMP_PATH: &'static str = "patchcode.spv";
        const TMP_OUTFILE: &'static str = "outfile.spv";
        std::fs::write(TMP_PATH, bytemuck::cast_slice(module.as_slice())).unwrap();
        //call the linker
        match std::process::Command::new("spirv-link")
            .arg("-o")
            .arg(TMP_OUTFILE)
            .arg(&self.src_file)
            .arg(TMP_PATH)
            .output()
        {
            Ok(_) => {
                let codebuffer = std::fs::read(TMP_OUTFILE).unwrap();
                let module = rspirv::dr::load_bytes(&codebuffer).unwrap();
                Some(module)
            }
            Err(e) => {
                log::error!("Failed to link module: {e:?}");
                None
            }
        }
    }
}

impl Patcher {
    pub fn new(device: Arc<marpii::context::Device>) -> Self {
        let (send, recv) = std::sync::mpsc::channel();

        //NOTE: PreSend the first shader module, by just loading the base shader without patches.
        let base_shader = ShaderModule::new_from_bytes(&device, BASE_SHADER)
            .expect("Could not build base-shader module!");

        send.send(OoS::new(base_shader))
            .expect("Failed to send initial base shader!");

        let mut watcher = Hotwatch::new_with_custom_delay(Duration::from_millis(500))
            .expect("Could not create file watcher!");

        if !Path::new("volasrc/sdf.vola").exists() {
            panic!("File sdf.vola does not exist!");
        }

        //touch the file

        let patcher =
            spv_patcher::Module::new(BASE_SHADER.to_vec()).expect("Could not load basecode");
        //prepare the base shader to be linked at some point

        let linkable_module = patcher
            .patch()
            .patch(MakeFunctionImport {
                name: "eval_sdf".to_owned(),
                identify_by: FuncIdent::Name("eval_sdf".to_owned()),
            })
            .unwrap()
            .patch(MakeFunctionImport {
                name: "eval_albedo".to_owned(),
                identify_by: FuncIdent::Name("eval_albedo".to_owned()),
            })
            .unwrap()
            .patch(MakeFunctionImport {
                name: "eval_gradient".to_owned(),
                identify_by: FuncIdent::Name("eval_gradient".to_owned()),
            })
            .unwrap();

        //write the module to a file that will be read by at link-time
        let linker = Linker::new(&linkable_module.unwrap_module());

        let file_path = std::path::PathBuf::from("volasrc/sdf.vola");

        watcher
            .watch("volasrc/", move |ev: Event| {
                let start = Instant::now();
                match std::panic::catch_unwind(|| {
                    if !ev.kind.is_modify() {
                        return;
                    }

                    let mut cpipeline = volac::Pipeline {
                        backend: Box::new(volac::backends::Spirv::new(volac::Target::buffer())),
                        late_cne: false,
                        ..volac::Pipeline::new()
                    };

                    let mut fpipeline = volac::Pipeline {
                        backend: Box::new(volac::backends::Spirv::new(volac::Target::file(
                            &"dump",
                        ))),
                        late_cne: false,
                        ..volac::Pipeline::new()
                    };

                    //First of all, try to run the compiler
                    let module = match cpipeline.execute_on_file(&file_path) {
                        Ok(t) => match t {
                            volac::Target::Buffer(b) => b,
                            volac::Target::File(f) => std::fs::read(f).unwrap(),
                        },
                        Err(e) => {
                            log::error!("Failed to compile sdf.vola: {e}");
                            return;
                        }
                    };

                    fpipeline.execute_on_file(&file_path).unwrap();

                    //now, try to inject all fields, based on the name using the patcher for our base module
                    let _patch = patcher.patch();

                    let linked_module = if let Some(m) = linker.link_new(&module) {
                        m
                    } else {
                        log::error!("Failed to link module!");
                        return;
                    };

                    let new_module_code = linked_module.assemble();

                    match ShaderModule::new_from_bytes(
                        &device,
                        bytemuck::cast_slice(&new_module_code),
                    ) {
                        Ok(sm) => {
                            let _ = send.send(sm.into());
                        }
                        Err(e) => {
                            log::error!("Could not build shader module for patched shader: {e}")
                        }
                    }
                }) {
                    Ok(_) => {
                        println!(
                            "Successfuly patched in {}ms",
                            start.elapsed().as_secs_f32() * 1000.0
                        );
                    }
                    Err(_e) => println!("Patching failed with panic"),
                }
            })
            .expect("Could not schedule sdf-file watcher!");

        Self {
            recv,
            hotwatch: watcher,
        }
    }

    pub fn fetch_new_module(&mut self) -> Option<OoS<ShaderModule>> {
        match self.recv.try_recv() {
            Ok(new) => Some(new),
            Err(TryRecvError::Disconnected) => {
                log::error!("Patch receiver is disconnected, restart the application!");
                None
            }
            _ => None,
        }
    }
}
